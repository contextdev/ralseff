from django.contrib import admin
from django.utils.html import mark_safe
from .models import Subscription, SubcriptionPayment, Course, Class, Assistance
from django.http import HttpResponseRedirect


class SubcriptionPaymentInlineAdmin(admin.StackedInline):
    model = SubcriptionPayment


class SubcriptionAdmin(admin.ModelAdmin):
    list_display = ['pk', 'client', 'total', 'total_paid', 'total_remain', 'fees_paid', 'ingresar_pago']
    search_fields = ['client__first_name', 'client__last_name']


    def total_paid(self, obj):
        return obj.total_paid
    total_paid.short_description = 'Total Pagado'

    def total_remain(self, obj):
        return obj.total_remain
    total_remain.short_description = 'Saldo a Pagar'

    def fees_paid(self, obj):
        return obj.fees_paid
    fees_paid.short_description = 'Cuotas Pagas'


    readonly_fields = ['total', 'total_remain', 'fees_paid', ]
    inlines = [SubcriptionPaymentInlineAdmin, ]


    def ingresar_pago(self, obj):
        # <a href="{% url admin:mode_change object.id %}?next={{ currentUrl }}">modify object</a>
        # return mark_safe('<a href="/course/subcriptionpayment/add/?subscription=%s&next=/course/subscription/">Ingresar Pago</a>' % obj.pk)
        return mark_safe('<a href="/course/subcriptionpayment/add/?subscription=%s&next=/print-payment-detail/2/" target="_blank">Ingresar Pago</a>' % obj.pk)

class SubsriptionPaymentAdmin(admin.ModelAdmin):

    def response_post_save_add(self, request, obj):
        if "next" in request.GET:
            return HttpResponseRedirect('/print-payment-detail-close-after/2/')
        return super().response_post_save_add(request, obj)


class ClassModelAdmin(admin.ModelAdmin):
    list_display = ['pk', 'course', 'date']
    list_filter = ('course',)

    def response_post_save_add(self, request, obj):
        return HttpResponseRedirect('/course/assistance/?class_obj__id__exact=%s' % (obj.pk,))



class AssistanceModelAdmin(admin.ModelAdmin):
    list_display = ['user', 'is_present']
    list_filter = ('class_obj__course', 'class_obj')
    list_display_links = None
    list_editable = ('is_present',)

    class Media:
        css = {
             'all': ('admin/css/assistance.css',)
        }


admin.site.register(Class, ClassModelAdmin)
admin.site.register(Assistance, AssistanceModelAdmin)

admin.site.register(Course)
admin.site.register(SubcriptionPayment, SubsriptionPaymentAdmin)
admin.site.register(Subscription, SubcriptionAdmin)



