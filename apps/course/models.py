from apps.person.models import Client
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.generic.models import GenericModel
from django.db.models import Sum

from apps.person.models import Client

from django.dispatch import receiver
from django.db.models.signals import post_save


class Course(GenericModel):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    price = models.FloatField(default=0)

    class Meta:
        verbose_name = _('Course')
        verbose_name_plural = _('Courses')

    def __str__(self):
        return self.name


from django.utils import timezone

class Class(GenericModel):
    course = models.ForeignKey(Course, related_name='classes')
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "%s - N° %s - %s" %(self.course, self.pk, self.date)

    class Meta:
        verbose_name = _('Class')
        verbose_name_plural = _('Classes')


class Assistance(GenericModel):
    class_obj = models.ForeignKey(Class, related_name='assistances')
    user = models.ForeignKey(Client, related_name='assistances')
    is_present = models.BooleanField(default=True)

    def __str__(self):
        return "%s: %s" % (self.user, _("Present") if self.is_present else _("Absent"))

    class Meta:
        verbose_name = _("Assistance")
        verbose_name_plural = _("Assistances")


@receiver(post_save, sender=Class)
def notification_post_save(sender, *args, **kwargs):
    class_obj = kwargs['instance']
    if kwargs['created']:
        for subscription in class_obj.course.subscriptions.all():
            Assistance(class_obj=class_obj, user=subscription.client).save()


class Subscription(GenericModel):
    course = models.ForeignKey(Course, related_name='subscriptions')
    client = models.ForeignKey(Client, related_name='subscriptions')
    month_fee = models.FloatField(choices=((500.0, '500'), (550.0, '550')), verbose_name='monto cuota')

    @property
    def total(self):
        if self.month_fee:
            return 5 * self.month_fee
        return 0

    @property
    def total_paid(self):
        payment_amount = self.payments.all().aggregate(Sum('amount'))['amount__sum']
        return payment_amount

    @property
    def total_remain(self):
        # payment_amount = self.payments.all().aggregate(Sum('price'))['price__sum']
        if self.total and self.total_paid:
            return self.total - self.total_paid
        return 0

    @property
    def fees_paid(self):
        if self.total_paid and self.month_fee:
            return int(self.total_paid / self.month_fee)
        return 0

    def __str__(self):
        return str(self.course) + " - " + str(self.client)


class SubcriptionPayment(GenericModel):
    amount = models.FloatField()
    subscription = models.ForeignKey(Subscription, related_name='payments')

