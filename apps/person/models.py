from django.db import models
from apps.generic.models import GenericModel
from django.core import urlresolvers
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import ugettext_lazy as _

# from apps.course.models import Course

class Person(GenericModel):
    class Meta:
        ordering = ['last_name', 'first_name']

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone = PhoneNumberField(help_text="e.g '+41524204242'", null=True, blank=True)
    address = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return "%s, %s" %(self.last_name.title(), self.first_name.title())


class Client(Person):
    # courses = models.ManyToManyField(Course, related_name='students', blank=True, null=True)

    def get_absolute_url(self):
        return urlresolvers.reverse('admin:person_patient_change', args=(self.id,))

    def get_full_name(self):
        return "%s, %s" %(self.last_name, self.first_name)

    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')
        ordering = ['last_name', 'first_name']

    def __str__(self):
        return self.first_name + " " + self.last_name
