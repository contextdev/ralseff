from django.contrib import admin

#from django.conf import settings
#from django.contrib.auth.admin import UserAdmin
#from django.contrib.auth.models import User
#from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site

from .models import Client

USER_MODEL_ADMIN_EXCLUDE = ['is_staff', 'is_superuser', 'user_permissions']


class ClientAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'phone']
    search_fields = ['first_name', 'last_name']


# class UserAdmin(admin.ModelAdmin):
#
#     add_form_template = 'admin/auth/user/add_form.html'
#     change_user_password_template = None
#     fieldsets = (
#         (None, {'fields': ('username', 'password')}),
#         ('Permissions', {'fields': ('is_active', 'groups')}),
#     )
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': ('username', 'password1', 'password2'),
#             }),
#     )
#     form = UserChangeForm
#     add_form = UserCreationForm
#     change_password_form = AdminPasswordChangeForm
#     list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff')
#     list_filter = ('is_active', 'groups')
#     search_fields = ('username', 'first_name', 'last_name', 'email')
#     ordering = ('username',)
#     filter_horizontal = ('groups', 'user_permissions',)
#
#
# if settings.SITE_ID != 1:
#     admin.site.unregister(User)
#     admin.site.register(User, UserAdmin)

admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)
admin.site.register(Client, ClientAdmin)

