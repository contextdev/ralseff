# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-27 19:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0005_auto_20160327_1900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='date',
            field=models.DateField(blank=True),
        ),
    ]
