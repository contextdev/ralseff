from django.db import models
from django.utils import timezone
from apps.person.models import Client

from apps.generic.models import GenericModel
from apps.course.models import Course
from django.db.models import Sum
from django.utils.translation import ugettext_lazy as _



class Bill(GenericModel):
    date = models.DateField(default=timezone.now, blank=True)
    detail = models.TextField(blank=True)

    client = models.ForeignKey(Client, related_name='billies')

    @property
    def total_paid(self):
        payment_amount = self.payments.all().aggregate(Sum('price'))['price__sum']
        return payment_amount if payment_amount else 0

    class Meta:
        verbose_name = _('Bill')
        verbose_name_plural = _('Billies')



class CourseBill(Bill):
    course = models.ForeignKey(Course)

    @property
    def total(self):
        return self.course.price

    @property
    def total_remaining(self):
        #import pdb; pdb.set_trace()
        return self.total - self.total_paid


class ItemBill(GenericModel):
    detail = models.CharField(max_length=100, blank=True)
    quantity = models.PositiveIntegerField(default=1)
    unit_price = models.FloatField()
    total_price = models.FloatField()
    bill = models.ForeignKey(Bill, related_name='items')

    class Meta:
        verbose_name = _('Detail Bill')
        verbose_name_plural = _('Details Bill')

    def save(self, *args, **kwargs):
        self.total_price = self.quantity * self.unit_price
        super(ItemBill, self).save(*args, **kwargs)


class Payment(GenericModel):
    date = models.DateField(blank=True)
    price = models.FloatField()
    detail = models.CharField(max_length=255, blank=True)
    bill = models.ForeignKey(Bill, related_name='payments')

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')
