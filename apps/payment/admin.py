from django.contrib import admin

from .models import Bill, ItemBill, Payment, CourseBill
from django.db.models import Sum
from django.utils import timezone
from django.core.urlresolvers import reverse


class ItemBillInLine(admin.TabularInline):
    #exclude = ('total_price' )
    readonly_fields = ('total_price',)
    extra = 0
    min_num = 0
    fk_name = 'bill'
    model = ItemBill


class PaymentInLine(admin.TabularInline):
    extra = 1
    fk_name = 'bill'
    model = Payment


def make_payed(modeladmin, request, queryset):
        queryset.update(status='1')

make_payed.short_description = 'Mark selected billies as payed.'


class BillAdmin(admin.ModelAdmin):
    # list_display = ('total', 'remaining')
    exclude = ['pay_date',]
    # readonly_fields = ('total', 'remaining', 'created')
    list_filter = ('client',)
    actions = [make_payed]
    inlines = [ItemBillInLine, PaymentInLine ]

    # def remaining(self, instance):
    #     try:
    #         payment_amount = instance.payments.all().aggregate(Sum('price'))['price__sum']
    #         total_amount = instance.items.all().aggregate(Sum('total_price'))['total_price__sum']
    #         return (total_amount - payment_amount) if payment_amount else total_amount
    #     except:
    #         return '0.00'
    #
    # def total(self, instance):
    #     try:
    #         total_amount = instance.items.all().aggregate(Sum('total_price'))['total_price__sum']
    #         return total_amount
    #     except:
    #         return '0.00'

from django.utils.html import mark_safe

from django.conf import settings

class CourseBillAdmin(admin.ModelAdmin):
    inlines = [PaymentInLine, ]
    exclude = ['detail']
    readonly_fields = ('total', 'total_remaining', 'total_paid', 'created', 'link')

    list_display = ['pk', 'total', 'total_paid', 'total_remaining', 'link',]

    def link(self, obj):
        if obj.pk:
            js = """
                    var newWindow = window.open();
                    var xmlhttp;
                    var ajax_content = '';
                    xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
                            if(xmlhttp.status == 200){
                                ajax_content = xmlhttp.responseText;
                                newWindow.document.open('text/html', target='_blank');
                                newWindow.document.write(ajax_content);
                                newWindow.print();
                                newWindow.close();
                            }else if(xmlhttp.status == 400) {
                                alert('There was an error 400');
                            }
                            else{
                                alert('something else other than 200 was returned');
                            }
                        }
                    }
                    xmlhttp.open('GET', '%s%s', true);
                    xmlhttp.send();
            """ %(settings.DOMAIN_URL, reverse('print-payment-detail', kwargs={'pk':obj.pk}))

            return mark_safe('<a onclick="%s">Imprimir</a>' % js)
        else:
            return None

    # def remaining(self, instance):
    #     try:
    #         payment_amount = instance.payments.all().aggregate(Sum('price'))['price__sum']
    #         total_amount = instance.course.price
    #         # total_amount = instance.items.all().aggregate(Sum('total_price'))['total_price__sum']
    #         return (total_amount - payment_amount) if payment_amount else total_amount
    #     except:
    #         return '0.00'
    #
    # def total(self, instance):
    #     try:
    #         total_amount = instance.course.price
    #         return total_amount
    #     except:
    #         return '0.00'

#admin.site.register(Bill, BillAdmin)
admin.site.register(CourseBill, CourseBillAdmin)


