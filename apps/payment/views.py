from django.shortcuts import render
from wkhtmltopdf.views import PDFTemplateView, PDFResponse
from django.views.generic.detail import DetailView
from . import models
from django.utils import timezone


class PaymentDetailView(DetailView):

    model = models.CourseBill

    def get_context_data(self, **kwargs):
        context = super(PaymentDetailView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context



class PaymentDetailViewToPdf(PaymentDetailView, PDFTemplateView):
    filename = 'my_pdf.pdf'
    show_content_in_browser = True
    template_name = 'payment/coursebill_detail.html'

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        return super().get_context_data(**kwargs)

    def render_to_response(self, context, **response_kwargs):
        if self.request.user:
            context['auth_user'] = self.request.user
            return super(PaymentDetailViewToPdf, self).render_to_response(context, **response_kwargs)
        else:
            return super(PaymentDetailViewToPdf, self).render_to_response(context, **response_kwargs)
