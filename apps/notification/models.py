from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string

from apps.generic.models import GenericModel

from .managers import NotificationManager


class Notification(GenericModel):
    message = models.TextField()
    user_ct = models.ForeignKey(ContentType, related_name='usernotifications')
    user_id = models.PositiveIntegerField()
    user_obj = generic.GenericForeignKey('user_ct', 'user_id')

    content_type = models.ForeignKey(ContentType, related_name='notifications')
    object_id = models.PositiveIntegerField()
    obj = generic.GenericForeignKey('content_type', 'object_id')

    objects = NotificationManager()

    def get_url(self):
        if self.obj:
            return self.obj.get_absolute_url()
        else:
            return ''


@receiver(post_save, sender=Notification)
def notification_post_save(sender, *args, **kwargs):
    notification = kwargs['instance']
    if kwargs['created']:
        # Send notification
        if notification.user_obj and notification.user_obj.email:
            context = {}
            context['notification'] = notification
            message = render_to_string('notification/emails/notification.html', context)
            msg = EmailMultiAlternatives('Reservation - Notifications',
                                         message,
                                         settings.DEFAULT_FROM_EMAIL,
                                         [notification.user_obj.email])
            msg.attach_alternative(message, 'text/html')
            msg.send()
