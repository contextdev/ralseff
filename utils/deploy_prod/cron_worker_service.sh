#!/bin/bash
NAME="ralseff_cron_worker"
ENVNAME=environment                                                     # Name of virtualenv
DJANGODIR=/webapps/ralseff/ralseff
USER=ralseff                                                        # the user to run as
GROUP=webapps                                                           # the user to run as
DJANGO_SETTINGS_MODULE=ralseff.settings.production                  # which settings file should Django use

cd $DJANGODIR
source ../$ENVNAME/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
      
exec celery -A ralseff worker -l info

