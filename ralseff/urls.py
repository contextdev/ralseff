from django.conf.urls import url, include, patterns
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

from apps.payment.views import PaymentDetailView, PaymentDetailViewToPdf


admin.site.site_url = None

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^', admin.site.urls),


    url(r'^payment-detail/(?P<pk>[0-9]+)/$', login_required(PaymentDetailView.as_view()), name='payment-detail'),
    url(r'^print-payment-detail/(?P<pk>[0-9]+)/$', login_required(PaymentDetailView.as_view(template_name='payment/print_coursebill_detail.html')), name='payment-detail'),
    url(r'^print-payment-detail-close-after/(?P<pk>[0-9]+)/$', login_required(PaymentDetailView.as_view(template_name='payment/print_coursebill_detail_close_after.html')), name='payment-detail'),
    url(r'^payment-detail-pdf/(?P<pk>[0-9]+)/$', login_required(PaymentDetailViewToPdf.as_view()), name='pdf'),
    url(r'^payment-detail/(?P<pk>[0-9]+)/$',
        login_required(PaymentDetailView.as_view()),
        name='payment-detail'),
    url(r'^print-payment-detail/(?P<pk>[0-9]+)/$',
        login_required(PaymentDetailView.as_view(template_name='payment/print_coursebill_detail.html')),
        name='print-payment-detail'),
    url(r'^payment-detail-pdf/(?P<pk>[0-9]+)/$',
        login_required(PaymentDetailViewToPdf.as_view()),
        name='payment-pdf'),
    # url(r'^occurrences_list_to_pdf/(?P<calendar_slug>[-\w]+)/$',
    #     login_required(OccurrenceListToPdf.as_view()), name='occurrences_list_to_pdf'),
]


if settings.DEBUG:
    urlpatterns += patterns('',
                           url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                           ) + staticfiles_urlpatterns() + urlpatterns