from .base import *


DEBUG = True

TEMPLATE_DEBUG = DEBUG


# INSTALLED_APPS += (
#     'django_extensions',   # for Ipython Notebook
#     'debug_toolbar',
# )

SITE_ID = 1

DOMAIN_URL = 'http://127.0.0.1:8000'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(filename)s][%(funcName)s][%(lineno)d][%(name)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'reservation.log',
            'formatter': 'verbose'
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'logs/task-debug.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':'DEBUG',
        },
        'debug': {
            'handlers': ['debug'],
            'level': 'DEBUG',
        },
    }
}