from .base import *

DEBUG = False

TEMPLATE_DEBUG = DEBUG

SITE_ID = 1

DOMAIN_URL = 'http://ralseff.context.com.ar'


ALLOWED_HOSTS = ["*"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}